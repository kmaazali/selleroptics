<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class Superadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::where('email',auth()->user()->email)->first();
        if ($user &&  $user->role_id == 1){
            return $next($request);
        }
        return response()->json('You are unauthorized.');
    }
}
