<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Package;
use App\Payment;
use Carbon\Carbon;

class PaymentController extends Controller
{
    public function vaultCreditCard(Request $request){

        $paypal_auth_token = 'access_token$sandbox$wr53qzps6g353pc6$5b0dbb4624933dfc55ecabedc1119b59';
        $user_id = auth()->user()->id;
        $clientId = env('Paypal_Client_Id');
        $secret = env('Paypal_Secret_Key');
        $ch = curl_init();
        $data = '{
            "number": "'.$request['card_number'].'",
            "type": "visa",
            "expire_month": '.$request['expire_month'].',
            "expire_year": '.$request['expire_year'].',
            "cvv2": "'.$request['cvv2'].'",
            "first_name": "'.$request['first_name'].'",
            "last_name": "'.$request['last_name'].'",
            "billing_address": {
                "line1": "'.$request['line1'].'",
              "city": "'.$request['city'].'",
              "country_code": "'.$request['country_code'].'",
              "postal_code": "'.$request['postal_code'].'",
              "state": "'.$request['state'].'",
              "phone": "'.$request['phone'].'"
            },
            "external_customer_id": "'.$user_id.'"
        }';

        // dd($encoded_data);
        curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/vault/credit-cards/");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLVERSION , 6); //NEW ADDITION
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{$data}");

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:application/json", 
            "Authorization:Bearer ".$paypal_auth_token,
            "Authorization:Basic QWJrbndESHFoZUVZTTEyNmN1eG1waWhCb3FUUkVSbnRmamJyNXlFNGM3bTZTSnc3dXBINU5aV2pPcFltc09TVzE5V1lEUi1Dd25uYVo4VzY6RUczTjQ2RUMyMzZSR2ctZTk2STc5ZFlPSkV4a1N6MXZHNGlKa2RIYjM0eUlVZklMcmI3Y2dHak16T3VqTlhPY1FaX1JhYXRVRHdKVGhadjI="
            )
        );

        $result = curl_exec($ch);

        if(empty($result))die("Error: No response.");
        else
        {
            $json = json_decode($result);
            // print_r($json->access_token);
            return response()->json($json);
        }
        
        curl_close($ch);

    }

    public function vaultPayment(Request $request){

        $paypal_auth_token = 'access_token$sandbox$wr53qzps6g353pc6$5b0dbb4624933dfc55ecabedc1119b59';
        $user_id = $request['user_id'];
        $clientId = env('Paypal_Client_Id');
        $secret = env('Paypal_Secret_Key');
        $ch = curl_init();
        $data = '{
            "intent": "sale",
            "payer": {
              "payment_method": "credit_card",
              "funding_instruments": [
              {
                "credit_card_token": {
                  "credit_card_id": "'.$request['card_id'].'",
                  "external_customer_id": "'.$user_id.'"
                }
              }]
            },
            "transactions": [
            {
              "amount": {
                "total": "'.$request['amount'].'",
                "currency": "USD"
              },
              "description": "Payment by vaulted credit card to amaxza."
            }]
          }';

        // dd($encoded_data);
        curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/payments/payment");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLVERSION , 6); //NEW ADDITION
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{$data}");

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:application/json", 
            "Authorization:Bearer ".$paypal_auth_token,
            "Authorization:Basic QWJrbndESHFoZUVZTTEyNmN1eG1waWhCb3FUUkVSbnRmamJyNXlFNGM3bTZTSnc3dXBINU5aV2pPcFltc09TVzE5V1lEUi1Dd25uYVo4VzY6RUczTjQ2RUMyMzZSR2ctZTk2STc5ZFlPSkV4a1N6MXZHNGlKa2RIYjM0eUlVZklMcmI3Y2dHak16T3VqTlhPY1FaX1JhYXRVRHdKVGhadjI="
            )
        );

        $result = curl_exec($ch);

        if(empty($result))die("Error: No response.");
        else
        {
            $json = json_decode($result);
            // print_r($json->access_token);
            // $url = $json->links{0}->{'href'};
            // $json_get = $this->paymentExecute($url);
            // ->payer->funding_instruments->credit_card_token->credit_card_id
            $card_id = $json->payer->funding_instruments[0]->credit_card_token->credit_card_id;
            $payment_id = $json->id;
            // dd($json->payer->funding_instruments[0]->credit_card_token->credit_card_id);
            // dd($json);
            $package = Package::select('_id')->where('name', 'Diamond')->pluck('_id');

            $payment = Payment::create([
                'credit_card_id' => $card_id,
                'payment_id' => $payment_id,
                'external_customer_id' => $user_id,
                'package_id' => $package[0]
            ]);
            // dd($package[0]);
            return response()->json($payment);
        }
        
        curl_close($ch);
    }

    public function vaultPaymentCurl(Request $request){

      $paypal_auth_token = 'access_token$sandbox$wr53qzps6g353pc6$5b0dbb4624933dfc55ecabedc1119b59';
      $user_id = $request['user_id'];
      $clientId = env('Paypal_Client_Id');
      $secret = env('Paypal_Secret_Key');
      $ch = curl_init();
      $data = '{
          "intent": "sale",
          "payer": {
            "payment_method": "credit_card",
            "funding_instruments": [
            {
              "credit_card_token": {
                "credit_card_id": "'.$request['card_id'].'",
                "external_customer_id": "'.$user_id.'"
              }
            }]
          },
          "transactions": [
          {
            "amount": {
              "total": "'.$request['amount'].'",
              "currency": "USD"
            },
            "description": "Payment by vaulted credit card to amaxza."
          }]
        }';

      // dd($encoded_data);
      curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/payments/payment");
      curl_setopt($ch, CURLOPT_HEADER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSLVERSION , 6); //NEW ADDITION
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
      curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "{$data}");

      // Set HTTP Header for POST request 
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type:application/json", 
          "Authorization:Bearer ".$paypal_auth_token,
          "Authorization:Basic QWJrbndESHFoZUVZTTEyNmN1eG1waWhCb3FUUkVSbnRmamJyNXlFNGM3bTZTSnc3dXBINU5aV2pPcFltc09TVzE5V1lEUi1Dd25uYVo4VzY6RUczTjQ2RUMyMzZSR2ctZTk2STc5ZFlPSkV4a1N6MXZHNGlKa2RIYjM0eUlVZklMcmI3Y2dHak16T3VqTlhPY1FaX1JhYXRVRHdKVGhadjI="
          )
      );

      $result = curl_exec($ch);

      if(empty($result))die("Error: No response.");
      else
      {
        $json = json_decode($result);
        // print_r($json->access_token);
        // $url = $json->links{0}->{'href'};
        // $json_get = $this->paymentExecute($url);
        $card_id = $json->payer->funding_instruments[0]->credit_card_token->credit_card_id;
        $payment_id = $json->id;

        $package = Package::select('_id')->where('name', 'Diamond')->pluck('_id');


        $payment = Payment::create([
            'credit_card_id' => $card_id,
            'payment_id' => $payment_id,
            'external_customer_id' => $user_id,
            'package_id' => $package[0]
        ]);
        // dd($package[0]);
        return response()->json($payment);
      }
      
      curl_close($ch);
  }

    public function paymentExecute($url){
        // $user_id = auth()->user()->id;
        $clientId = env('Paypal_Client_Id');
        $secret = env('Paypal_Secret_Key');
        $paypal_auth_token = 'access_token$sandbox$wr53qzps6g353pc6$5b0dbb4624933dfc55ecabedc1119b59';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLVERSION , 6); //NEW ADDITION
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);

        // Set HTTP Header for POST request 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type:application/json", 
            "Authorization:Bearer ".$paypal_auth_token,
            "Authorization:Basic QWJrbndESHFoZUVZTTEyNmN1eG1waWhCb3FUUkVSbnRmamJyNXlFNGM3bTZTSnc3dXBINU5aV2pPcFltc09TVzE5V1lEUi1Dd25uYVo4VzY6RUczTjQ2RUMyMzZSR2ctZTk2STc5ZFlPSkV4a1N6MXZHNGlKa2RIYjM0eUlVZklMcmI3Y2dHak16T3VqTlhPY1FaX1JhYXRVRHdKVGhadjI="
            )
        );

        $result = curl_exec($ch);

        if(empty($result))die("Error: No response.");
        else
        {
            $json = json_decode($result);

            return response()->json($json);
        }
        
        curl_close($ch);
    }

    public function packageSelect(Request $request){
        $user_id = auth()->user()->id;
        
        $user = User::find($user_id);
        $user->package = $request['package'];
        $user->save();

        return $user;
    }
}
