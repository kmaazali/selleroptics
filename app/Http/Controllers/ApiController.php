<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterAuthRequest;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\SellerAccount;
use App\Marketplace;
use App\Pricer;
use App\Addresses;
use App\Category;
use App\Package;
use App\GradingNote;
use App\AddItem;
use App\Note;

class ApiController extends Controller
{
    public $loginAfterSignUp = true;

    public function register(RegisterAuthRequest $request){

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        if ($this->loginAfterSignUp) {
            return $this->login($request);
        }

        return response()->json([
            'success' => true,
            'data' => $user
        ], 200);
    }

    public function login(Request $request){

        $input = $request->only('email', 'password');
        $jwt_token = null;

        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], 401);
        }

        return response()->json([
            'success' => true,
            'token' => $jwt_token,
        ]);
    }

    public function logout(Request $request){

        $this->validate($request, [
            'token' => 'required'
        ]);

        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }

    public function getAuthUser(Request $request){

        $this->validate($request, [
            'token' => 'required'
        ]);

        $user = JWTAuth::authenticate($request->token);

        return response()->json(['user' => $user]);
    }

    public function selectMarketplace(Request $request){
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $user->is_platform_connected = true;
        $user->save();
        $market = Marketplace::where('marketplace_id',$request['marketplace_id'])->first();
        $mkt_det = new SellerAccount();
        $mkt_det->user_id = $user_id;
        $mkt_det->marketplace_id = $market->marketplace_value;
        $mkt_det->seller_id = $request['seller_id'];
        $mkt_det->mws_authorization_token = $request['mws_authorization_token'];
        $mkt_det->save();
        $this->verifySignup($user_id);
        return response()->json([
            'success' => true,
            'data' => $mkt_det
        ], 200);
    }

    public function verifySignup($id){
        $user = User::find($id);
        $user->is_signup_complete = true;
        $user->save();
    }

    public function updatePricers(Request $request){
        $user_id=auth()->user()->id;
        $each_price=$request['list_price_value'];
        $each_price_unit=$request['list_price_unit'];
        $flat_price=$request['flat_price'];
        $switch = $request['is_pricer'];

        $pricer=Pricer::where('user_id',$user_id)->first();
        if($pricer) {
            $pricer->list_price_value = $each_price;
            $pricer->list_price_unit = $each_price_unit;
            $pricer->flat_price = $flat_price;
            $pricer->is_pricer = $switch;
            $pricer->save();
            return response()->json([
                'success' => true,
                'data' => $pricer
            ], 200);
        }
        else{
            $pricer=new Pricer();
            $pricer->list_price_value = $each_price;
            $pricer->list_price_unit = $each_price_unit;
            $pricer->flat_price = $flat_price;
            $pricer->user_id = $user_id;
            $pricer->is_pricer = $switch;
            $pricer->save();
            return response()->json([
                'success' => true,
                'data' => $pricer
            ], 200);
        }
    }

    public function getPricers($id){
        $pricer = Pricer::where('user_id', $id)->get();
        return response()->json([
            'success' => true,
            'data' => $pricer
        ], 200);
    }

    public function getAddresses(){
        $user_id = auth()->user()->id;
        // dd($user_id);
        $addresses = Addresses::where('user_id',$user_id)->get();
        return response()->json([
            'success' => true,
            'data' => $addresses
        ], 200);
    }

    public function addAddress(Request $request){
        $user_id = auth()->user()->id;
        $address = new Addresses();
        $address->name = $request['name'];
        $address->address_line_1 = $request['addressline1'];
        $address->address_line_2 = $request['addressline2'];
        $address->city = $request['city'];
        $address->zip_code = $request['zipcode'];
        $address->country = $request['country'];
        $address->state = $request['state'];
        $address->user_id = $user_id;
        $address->save();
        $addresses = Addresses::where('user_id',$user_id)->get();
        return response()->json([
            'success' => true,
            'data' => $addresses
        ], 200);
    }

    public function deleteAddress($id){
        $user_id = auth()->user()->id;
        Addresses::where('_id',$id)->delete();
        $addresses = Addresses::where('user_id',$user_id)->get();
        return response()->json([
            'success' => true,
            'data' => $addresses
        ], 200);
    }

    public function editAddress(Request $request, $id){

        Addresses::where('_id', $id)
                ->update(['name' => $request->input('name'),
                         'address_line_1'=>$request->input('address_line_1'),
                         'address_line_2'=>$request->input('address_line_2'),
                         'city'=>$request->input('city'),
                         'zip_code'=>$request->input('zip_code'),
                         'country'=>$request->input('country'),
                         'state'=>$request->input('state')]
                        );
        $address = Addresses::where('_id', $id)->get();
        return response()->json([
            'success' => true,
            'data' => $address
        ], 200);
    }

    public function postEditAddress(Request $request){

        $user_id = auth()->user()->id;
        $address = Addresses::where('_id',$request['id'])->first();
        $address->name = $request['name'];
        $address->address_line_1 = $request['addressline1'];
        $address->address_line_2 = $request['addressline2'];
        $address->city = $request['city'];
        $address->zip_code = $request['zipcode'];
        $address->country = $request['country'];
        $address->state = $request['state'];
        $address->save();
        $addresses = Addresses::where('user_id',$user_id)->get();
        return response()->json([
            'success' => true,
            'data' => $addresses
        ], 200);
    }

    public function gradeItem(Request $request){
        $user_id=auth()->user()->id;

        $grade = [];
        $grade['user_id'] = $user_id;
        $grade['title'] = $request['title'];
        $grade['image'] = $request['image'];
        $grade['asin'] = $request['asin'];
        $grade['listprice'] = $request['listprice'];
        $grade['category'] = $request['category'];
        $grade['myprice'] = $request['myprice'];
        $grade['salesrank'] = $request['salesrank'];
        $grade['currency'] = $request['currency'];
        $grade['eachlistprice'] = $request['eachlistprice'];
        $grade['mycost'] = $request['mycost'];
        $grade['feepreview'] = $request['feepreview'];
        $grade['manufacturer'] = $request['manufacturer'];
        $categories = Category::get();

        return response()->json([
            'success' => true,
            'data' => compact('grade','categories')
        ], 200);
        
    }

    public function setPackages($id){
        $user = User::find($id);
        $user->is_verification_complete = true;
        $user->save();

        return response()->json([
            'success' => true,
            'data' => $user
        ], 200);
    }

    public function addNote(Request $request){
        $user_id = auth()->user()->id;
        $fulfillment = $request['fulfillment'];
        $condition = $request['condition'];
        $subcondition = $request['subcondition'];
        $category = $request['category'];
        $note = $request['note'];

        $grading = new GradingNote();
        $grading->user_id = $user_id;
        $grading->fulfillment = $fulfillment;
        $grading->condition = $condition;
        $grading->subcondition = $subcondition;
        $grading->category = $category;
        $grading->note = $note;
        $grading->save();

        return response()->json([
            'success' => true,
            'data' => $grading
        ], 200);
    }

    public function getNotes($category){
        $user_id = auth()->user()->id;
        if($category != 'All Notes'){
            $grade = GradingNote::where('user_id', $user_id)->where('category', $category)->get();
            return response()->json([
                'success' => true,
                'data' => $grade
            ], 200);
        }
        else{
            $grade = GradingNote::where('user_id', $user_id)->get();
            return response()->json([
                'success' => true,
                'data' => $grade
            ], 200);
        }
        
    }

    public function getGradingNotes($condition){
        $user_id = auth()->user()->id;
        $grade = GradingNote::where('user_id', $user_id)->where('condition', $condition)->get();
        return response()->json([
            'success' => true,
            'data' => $grade
        ], 200);
    }

    public function getAllCategories(){
        $user_id = auth()->user()->id;
        $grade = GradingNote::where('user_id', $user_id)->get();
        return response()->json([
            'success' => true,
            'data' => $grade
        ], 200);
    }

    public function addItem(Request $request){
        $user_id = auth()->user()->id;
        $asin = $request['asin'];
        $list_price = $request['list_price'];
        $category = $request['category'];
        $image = $request['image'];
        $title = $request['title'];
        $rank = $request['rank'];
        $my_price = $request['my_price'];
        $quantity = $request['quantity'];
        $msku = $this->generateRandomString();
        $condition = $request['condition'];
        $note = $request['note'];
        $tax_code = $request['tax_code'];
        $my_cost = $request['my_cost'];
        $fees = $request['fees'];
        $profit_loss = $request['profit_loss'];
        $item_status = 1;

        $addItem = new AddItem();
        $addItem->user_id = $user_id;
        $addItem->asin = $asin;
        $addItem->list_price = $list_price;
        $addItem->category = $category;
        $addItem->image = $image;
        $addItem->title = $title;
        $addItem->rank = $rank;
        $addItem->my_price = $my_price;
        $addItem->quantity = $quantity;
        $addItem->msku = $msku;
        $addItem->condition = $condition;
        $addItem->note = $note;
        $addItem->tax_code = $tax_code;
        $addItem->my_cost = $my_cost;
        $addItem->fees = $fees;
        $addItem->profit_loss = $profit_loss;
        $addItem->item_status = $item_status;
        $addItem->save();
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function postNotes(Request $request){
        $user_id = auth()->user()->id;
        $condition = $request['condition'];
        $category = $request['category'];
        $note = $request['note'];
        $notes = Note::select('note')->where('user_id', $user_id)->where('condition', $condition)->get();
        dd($notes);
        if ($notes->isEmpty() || $notes != $note) {
            $Note = new Note();
            $Note->user_id = $user_id;
            $Note->category = $category;
            $Note->condition = $condition;
            $Note->note = $note;
            $Note->save();
            return response()->json([
                'success' => true,
                'data' => $Note
            ], 200);
        }
        else{
            return response()->json([
                'success' => true,
                'data' => 'Condition already exist'
            ], 200);
        }
    }

    public function fetchNotes($condition){
        $user_id = auth()->user()->id;
        $note = Note::select('note')->where('user_id', $user_id)->where('condition', $condition)->get();
        return response()->json([
            'success' => true,
            'data' => $note
        ], 200);
    }
}
