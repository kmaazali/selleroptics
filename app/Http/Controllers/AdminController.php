<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Mail\VerifyAccount;
use Auth;
use Tymon\JWTAuth\PayloadFactory;
use Tymon\JWTAuth\JWTManager as JWT;

class AdminController extends Controller
{
    public function getUsers(){
        // $user=User::with('role')->get();
        $user = User::wherein('role_id', [2,3])->paginate(5);
        // $user = User::with('role')->paginate(3);
        return response()->json($user);
    }

    public function blockUser($id){
        $user = User::find($id);
        $user->is_blocked=true;
        $user->save();
        return $user;
    }

    public function unblockUser($id){
        $user = User::find($id);
        $user->is_blocked=false;
        $user->save();
        return $user;
    }

    public function adminAddEmployee(Request $request){
        // dd(Auth::user()->id);
        $client_id = $request['client_id'];
        $validator = Validator::make($request->json()->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        // dd($client_id);
        $user = User::create([
            'name' => $request->json()->get('name'),
            'email' => $request->json()->get('email'),
            'password' => Hash::make($request->json()->get('password')),
            'phone' =>$request->json()->get('phone'),
            'role_id'=>3,
            'client_id'=>$client_id,
            'company_name' =>$request->json()->get('company_name'),
            'is_signup_complete'=>false,
        ]);


        $token = JWTAuth::fromUser($user);

        $user=User::where('email',$request->json()->get('email'))->first();

        $data = array(
            'name' => $user->name,
            'id' =>  $user->id,
            'email'=>$user->email,
        );

        return response()->json(['user'=>$user,'data'=>$data, 'token'=>$token,'success'=>true,'message'=>'Account Created Successfully'],201);
    }

    public function getUsersDropdown(){
        $user = User::where('role_id', 2)->get();
        return response()->json($user);
    }

    public function resetPassword(Request $request){
        $id = $request['id'];
        $pass = $request['password'];
        User::find($id)->update(['password' => Hash::make($pass)]);

        return response()->json('Password updated');
    }

    public function deleteUser(Request $request){
        $id = $request['id'];
        $user = User::find($id);
        // User::withTrashed()->restore();
        
        if ($user->role_id == 2) {
            $childUsers = User::select('_id')->where('client_id', $user->_id)->pluck('_id');
            // dd($childUsers);
            if(!empty($childUsers)){
                foreach ($childUsers as $childUser) {
                    User::find($childUser)->delete();
                }
                User::find($id)->delete();
                return response()->json('Client and Employee Deleted');
            }
            User::find($id)->delete();
            return response()->json('Client Deleted');
        }
        else{
            User::find($id)->delete();
            return response()->json('Employee Deleted');
        }
    }
}
