<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterAuthRequest;
use Illuminate\Http\Request;
use App\PushedItem;
use App\Pricer;
use App\Marketplace;
use App\SellerAccount;
use App\Traits\CommonTrait;
use Config;

class AmazonController extends Controller
{
    use CommonTrait;

    public function searchProduct(Request $request){
        $user_id = auth()->user()->id;
        $mkt = SellerAccount::where('user_id', $user_id)->first();
        // dd($mkt->marketplace_id);
        $query = $request['query'];
        $marketplace_id = $mkt->marketplace_id;
        $mws_token = $mkt->mws_auth_token;
        $seller_id = $mkt->seller_id;
        // $marketplace = Marketplace::where('marketplace_id',$marketplace_id)->first();
        $marketplace = $mkt->marketplace_id;
        
        // dd(env('AWS_ACCESS_KEY_ID'));

        date_default_timezone_set('UTC');
        define ("AWS_ACCESS_KEY_ID", env('AWS_ACCESS_KEY_ID'));
        define ("MARKETPLACE_ID", $marketplace);
        define ("AWS_SECRET_ACCESS_KEY", env('AWS_SECRET_ACCESS_KEY'));
        define ("SELLER_ID",$seller_id);

        $params = array(
            'AWSAccessKeyId' => AWS_ACCESS_KEY_ID,
            'Action' => "ListMatchingProducts",
            'Query' => $query,
            'MWSAuthToken'=>$mws_token,
            'MarketplaceId'=> MARKETPLACE_ID,
            'SellerId'=> SELLER_ID,
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp'=> date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version'=> "2011-10-01");


        $url_parts = array();
        foreach(array_keys($params) as $key)
            $url_parts[] = $key . "=" . str_replace('%7E', '~', rawurlencode($params[$key]));
        sort($url_parts);

        //Implode URL
        $url_string = implode("&", $url_parts);
        $string_to_sign = "GET\nmws.amazonservices.com\n/Products/2011-10-01\n" . $url_string;

        // Request Signing
        $signature = hash_hmac("sha256", $string_to_sign, AWS_SECRET_ACCESS_KEY, TRUE);

        // Base64 the signature and make URL safe
        $signature = urlencode(base64_encode($signature));

        $url = "https://mws.amazonservices.com/Products/2011-10-01" . '?' . $url_string . "&Signature=" . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $response = curl_exec($ch);


        //header('Content-Type: application/xml');
        //echo $response;

        // dd($response);
        $plainXML = $this->mungXML($response);
        $products = (json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)));
        $products=(json_decode($products,true));
        $products=($products["ListMatchingProductsResult"]["Products"]);
        return response()->json([
            'success' => true,
            'data' => $products
        ], 200);
    }

    public function getLowestPricedOfferings(Request $request){

        $query=$request['asin'];
        $user_id=auth()->user()->id;
        $mkt = SellerAccount::where('user_id', $user_id)->first();
        $marketplace_id = $mkt->marketplace_id;
        $mws_token = $mkt->mws_auth_token;
        $seller_id = $mkt->seller_id;
        // dd($mkt->marketplace_id);
        // $marketplace=Marketplace::where('_id',$marketplace_id)->first();
        $marketplace=$mkt->marketplace_id;
        // dd($marketplace);
        date_default_timezone_set('UTC');
        define ("AWS_ACCESS_KEY_ID", "AKIAJ3QLKPQ3SNMG743Q");
        define ("MARKETPLACE_ID", $marketplace);
        define ("AWS_SECRET_ACCESS_KEY",env("AWS_SECRET_ACCESS_KEY"));
        define ("SELLER_ID",$seller_id);

        $params = array(
            'ASIN' => $query,
            'AWSAccessKeyId' => AWS_ACCESS_KEY_ID,
            'ItemCondition' => "Used",
            'Action' => "GetLowestPricedOffersForASIN",
            'MWSAuthToken'=>$mws_token,
            'MarketplaceId'=> MARKETPLACE_ID,
            'SellerId'=> SELLER_ID,
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp'=> date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version'=> "2011-10-01");


        $url_parts = array();
        foreach(array_keys($params) as $key)
            $url_parts[] = $key . "=" . str_replace('%7E', '~', rawurlencode($params[$key]));
        sort($url_parts);

        //Implode URL
        $url_string = implode("&", $url_parts);
        $string_to_sign = "GET\nmws.amazonservices.com\n/Products/2011-10-01\n" . $url_string;

        // Request Signing
        $signature = hash_hmac("sha256", $string_to_sign, AWS_SECRET_ACCESS_KEY, TRUE);

        // Base64 the signature and make URL safe
        $signature = urlencode(base64_encode($signature));

        $url = "https://mws.amazonservices.com/Products/2011-10-01" . '?' . $url_string . "&Signature=" . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $response = curl_exec($ch);


        $plainXML = $this->mungXML($response);
        $arrayResult = (json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)));
        $prices=(json_decode($arrayResult,true));
        
        $title=$request['product_title'];
        $sales_rank=$request['sales_rank'];
        $category=$request['category'];
        $image=$request['image'];
        
        $pricersettings=Pricer::where('user_id',$user_id)->first();
        $shipping=0;
        $amount=0;

        foreach($prices["GetLowestPricedOffersForASINResult"]["Summary"]["LowestPrices"]["LowestPrice"] as $lowestprices) {
            if(isset($lowestprices["@attributes"]["condition"])){
            if ($lowestprices["@attributes"]["condition"] == "new" && $lowestprices["@attributes"]["fulfillmentChannel"] == "Amazon") {

                if ($lowestprices["@attributes"]["condition"]) {
                    $shipping = $lowestprices["Shipping"]["Amount"];
                    $amount = $lowestprices["ListingPrice"]["Amount"];

                }
            }
        }
           elseif ($prices["GetLowestPricedOffersForASINResult"]["Summary"]["LowestPrices"]["LowestPrice"]["@attributes"]["condition"]=="new"){
                $shipping=$prices["GetLowestPricedOffersForASINResult"]["Summary"]["LowestPrices"]["LowestPrice"]["Shipping"]["Amount"];
                $amount=$prices["GetLowestPricedOffersForASINResult"]["Summary"]["LowestPrices"]["LowestPrice"]["ListingPrice"]["Amount"];
           }
        }
        // dd($amount);
        $referralfee=0;
        $variableclosing=0;
        $fulfillmentfee=0;

        $estimatedprice=$this->getMyFeesEstimate($query,$shipping,$amount);
        if(!isset($estimatedprice["GetMyFeesEstimateResult"]["FeesEstimateResultList"]["FeesEstimateResult"]["Error"])) {
            $referralfee = 0;
            $variableclosing = 0;
            $fulfillmentfee = 0;
            foreach ($estimatedprice["GetMyFeesEstimateResult"]["FeesEstimateResultList"]["FeesEstimateResult"]["FeesEstimate"]["FeeDetailList"]["FeeDetail"] as $feedetail) {
                if ($feedetail["FeeType"] == "ReferralFee") {
                    $referralfee = $feedetail["FinalFee"]["Amount"];
                } elseif ($feedetail["FeeType"] == "VariableClosingFee") {
                    $variableclosing = $feedetail["FinalFee"]["Amount"];
                } elseif ($feedetail["FeeType"] == "FBAFees") {
                    $fulfillmentfee = $feedetail["FeeAmount"]["Amount"];
                }
            }
        }
        $manufacturer=$request['manufacturer'];


        return response()->json([
            'success' => true,
            'data' => $prices
        ], 200);
    }

    public function lowestOfferForAsin(Request $request){
        $query=$request['asin'];
        $user_id=auth()->user()->id;

        $mkt = SellerAccount::where('user_id', $user_id)->first();
        $marketplace_id = $mkt->marketplace_id;
        $mws_token = $mkt->mws_auth_token;
        $seller_id = $mkt->seller_id;
        // $marketplace=Marketplace::where('_id',$marketplace_id)->first();
        $marketplace=$mkt->marketplace_id;;
        // dd($marketplace);
        date_default_timezone_set('UTC');
        define ("AWS_ACCESS_KEY_ID", "AKIAJ3QLKPQ3SNMG743Q");
        define ("MARKETPLACE_ID", $marketplace);
        define ("AWS_SECRET_ACCESS_KEY",env("AWS_SECRET_ACCESS_KEY"));
        define ("SELLER_ID",$seller_id);


        $params = array(
            'ASINList.ASIN.1' => $query,
            'AWSAccessKeyId' => AWS_ACCESS_KEY_ID,
            'Action' => "GetLowestOfferListingsForASIN",
            'MWSAuthToken'=> $mws_token,
            'MarketplaceId'=> MARKETPLACE_ID,
            'SellerId'=> SELLER_ID,
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp'=> date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version'=> "2011-10-01");

        $url_parts = array();
        foreach(array_keys($params) as $key)
            $url_parts[] = $key . "=" . str_replace('%7E', '~', rawurlencode($params[$key]));
        sort($url_parts);

        //Implode URL
        $url_string = implode("&", $url_parts);
        $string_to_sign = "GET\nmws.amazonservices.com\n/Products/2011-10-01\n" . $url_string;

        // Request Signing
        $signature = hash_hmac("sha256", $string_to_sign, AWS_SECRET_ACCESS_KEY, TRUE);

        // Base64 the signature and make URL safe
        $signature = urlencode(base64_encode($signature));

        $url = "https://mws.amazonservices.com/Products/2011-10-01" . '?' . $url_string . "&Signature=" . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $response = curl_exec($ch);

        $plainXML = $this->mungXML($response);
        $arrayResult = (json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)));
        $prices=(json_decode($arrayResult,true));

        return response()->json([
            'success' => true,
            'data' => $prices
        ], 200);

    }

    public function getCompetitivePricing(Request $request){
        $query=$request['asin'];
        $user_id=auth()->user()->id;

        $mkt = SellerAccount::where('user_id', $user_id)->first();
        $marketplace_id = $mkt->marketplace_id;
        $mws_token = $mkt->mws_auth_token;
        $seller_id = $mkt->seller_id;
        // $marketplace=Marketplace::where('_id',$marketplace_id)->first();
        $marketplace=$mkt->marketplace_id;;

        date_default_timezone_set('UTC');
        define ("AWS_ACCESS_KEY_ID", "AKIAJ3QLKPQ3SNMG743Q");
        define ("MARKETPLACE_ID", $marketplace);
        define ("AWS_SECRET_ACCESS_KEY",env("AWS_SECRET_ACCESS_KEY"));
        define ("SELLER_ID",$seller_id);

        $params = array(
            'ASINList.ASIN.1' => $query,
            'AWSAccessKeyId' => AWS_ACCESS_KEY_ID,
            'Action' => "GetCompetitivePricingForASIN",
            'MWSAuthToken'=> $mws_token,
            'MarketplaceId'=> MARKETPLACE_ID,
            'SellerId'=> SELLER_ID,
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp'=> date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version'=> "2011-10-01");

        $url_parts = array();
        foreach(array_keys($params) as $key)
            $url_parts[] = $key . "=" . str_replace('%7E', '~', rawurlencode($params[$key]));
        sort($url_parts);

        $url_string = implode("&", $url_parts);
        $string_to_sign = "GET\nmws.amazonservices.com\n/Products/2011-10-01\n" . $url_string;

        $signature = hash_hmac("sha256", $string_to_sign, AWS_SECRET_ACCESS_KEY, TRUE);

        $signature = urlencode(base64_encode($signature));

        $url = "https://mws.amazonservices.com/Products/2011-10-01" . '?' . $url_string . "&Signature=" . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $response = curl_exec($ch);

        $plainXML = $this->mungXML($response);
        $arrayResult = (json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)));
        $prices=(json_decode($arrayResult,true));
        return response()->json([
            'success' => true,
            'data' => $prices
        ], 200);
    }

    public function listOrders(Request $request){

        $user_id=auth()->user()->id;

        $mkt = SellerAccount::where('user_id', $user_id)->first();
        $marketplace_id = $mkt->marketplace_id;
        $mws_token = $mkt->mws_auth_token;
        $seller_id = $mkt->seller_id;
        $marketplace=Marketplace::where('_id',$marketplace_id)->first();
        $marketplace=$marketplace->marketplace_value;

        date_default_timezone_set('UTC');
        define ("AWS_ACCESS_KEY_ID", env("AWS_ACCESS_KEY_ID"));
        define ("MARKETPLACE_ID", $marketplace);
        define ("AWS_SECRET_ACCESS_KEY",env("AWS_SECRET_ACCESS_KEY"));
        define ("SELLER_ID",$seller_id);
        define("ENDPOINT",$request['endpoint']);
        $params = array(
            'AWSAccessKeyId' => AWS_ACCESS_KEY_ID,
            'Action' => "ListOrders",
            'MWSAuthToken'=> $mws_token,
            'MarketplaceId.Id.1'=> MARKETPLACE_ID,
            'SellerId'=> SELLER_ID,
            $request['endpoint']=>date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", strtotime($request['datevalue'])),
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp'=> date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version'=> "2013-09-01");

        $url_parts = array();
        foreach(array_keys($params) as $key)
            $url_parts[] = $key . "=" . str_replace('%7E', '~', rawurlencode($params[$key]));
        sort($url_parts);

        $url_string = implode("&", $url_parts);
        $string_to_sign = "POST\nmws.amazonservices.com\n/Orders/2013-09-01\n" . $url_string;

        $signature = hash_hmac("sha256", $string_to_sign, AWS_SECRET_ACCESS_KEY, TRUE);

        $signature = urlencode(base64_encode($signature));

        $url = "https://mws.amazonservices.com/Orders/2013-09-01" . '?' . $url_string . "&Signature=" . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://mws.amazonservices.com/Orders/2013-09-01');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$url_string . "&Signature=" . $signature);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);


        $plainXML = $this->mungXML($response);
        $arrayResult = (json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)));
        $orders=(json_decode($arrayResult,true));
        return response()->json([
            'success' => true,
            'data' => $orders
        ], 200);
    }

    public function orderListNextToken(Request $request){
        
        $user_id=auth()->user()->id;

        $mkt = SellerAccount::where('user_id', $user_id)->first();
        $marketplace_id = $mkt->marketplace_id;
        $mws_token = $mkt->mws_auth_token;
        $seller_id = $mkt->seller_id;
        $marketplace=Marketplace::where('_id',$marketplace_id)->first();
        $marketplace=$marketplace->marketplace_value;

        date_default_timezone_set('UTC');
        define ("AWS_ACCESS_KEY_ID", env("AWS_ACCESS_KEY_ID"));
        define ("MARKETPLACE_ID", $marketplace);
        define ("AWS_SECRET_ACCESS_KEY",env("AWS_SECRET_ACCESS_KEY"));
        define ("SELLER_ID",$seller_id);
        define("ENDPOINT",$request['endpoint']);

        $params = array(
            'AWSAccessKeyId' => AWS_ACCESS_KEY_ID,
            'Action' => "ListOrdersByNextToken",
            'MWSAuthToken'=> $mws_token,
            'SellerId'=> SELLER_ID,
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp'=> date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version'=> "2013-09-01",
            'NextToken' =>$request['NextToken']);

        $url_parts = array();
        foreach(array_keys($params) as $key)
            $url_parts[] = $key . "=" . str_replace('%7E', '~', rawurlencode($params[$key]));
        sort($url_parts);


        $url_string = implode("&", $url_parts);
        $string_to_sign = "POST\nmws.amazonservices.com\n/Orders/2013-09-01\n" . $url_string;

        $signature = hash_hmac("sha256", $string_to_sign, AWS_SECRET_ACCESS_KEY, TRUE);

        $signature = urlencode(base64_encode($signature));

        $url = "https://mws.amazonservices.com/Orders/2013-09-01" . '?' . $url_string . "&Signature=" . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://mws.amazonservices.com/Orders/2013-09-01');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$url_string . "&Signature=" . $signature);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);


        $plainXML = $this->mungXML($response);
        $arrayResult = (json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)));
        $orders=(json_decode($arrayResult,true));
        return response()->json([
            'success' => true,
            'data' => $orders
        ], 200);
    }

    public function searchsingleasin(Request $request){
        $query=$request['query'];
        $user_id=auth()->user()->id;

        $mkt = SellerAccount::where('user_id', $user_id)->first();
        $marketplace_id = $mkt->marketplace_id;
        $mws_token = $mkt->mws_auth_token;
        $seller_id = $mkt->seller_id;
        $marketplace=Marketplace::where('_id',$marketplace_id)->first();
        $marketplace=$marketplace->marketplace_value;;
        date_default_timezone_set('UTC');
        define ("AWS_ACCESS_KEY_ID", env("AWS_ACCESS_KEY_ID"));
        define ("MARKETPLACE_ID", $marketplace);
        define ("AWS_SECRET_ACCESS_KEY",env("AWS_SECRET_ACCESS_KEY"));
        define ("SELLER_ID",$seller_id);

        

        $params = array(
            'AWSAccessKeyId' => AWS_ACCESS_KEY_ID,
            'Action' => "GetMatchingProductForId",
            'IdList.Id.1' => $query,
            'IdType' =>'ASIN',
            'MWSAuthToken'=>$mws_token,
            'MarketplaceId'=> MARKETPLACE_ID,
            'SellerId'=> SELLER_ID,
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp'=> date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version'=> "2011-10-01");
            
            
            $url_parts = array();
            foreach(array_keys($params) as $key)
            $url_parts[] = $key . "=" . str_replace('%7E', '~', rawurlencode($params[$key]));
            sort($url_parts);

            //Implode URL
            $url_string = implode("&", $url_parts);
        $string_to_sign = "GET\nmws.amazonservices.com\n/Products/2011-10-01\n" . $url_string;
        
        // Request Signing
        $signature = hash_hmac("sha256", $string_to_sign, AWS_SECRET_ACCESS_KEY, TRUE);
        
        // Base64 the signature and make URL safe
        $signature = urlencode(base64_encode($signature));
        
        $url = "https://mws.amazonservices.com/Products/2011-10-01" . '?' . $url_string . "&Signature=" . $signature;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $response = curl_exec($ch);
            
        
        //header('Content-Type: application/xml');
        //echo $response;
        
        $plainXML = $this->mungXML($response);
        $products = (json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)));
        $products = (json_decode($products,true));
        $products = ($products["GetMatchingProductForIdResult"]["Products"]);
        return response()->json([
            'success' => true,
            'data' => $products
        ], 200);
    }

    public function ListMatchingProduct(Request $request){
        $query=$request['query'];
        
        $user_id=auth()->user()->id;
        
        $mkt = SellerAccount::where('user_id', $user_id)->first();
        $marketplace_id = $mkt->marketplace_id;
        $mws_token = $mkt->mws_auth_token;
        $seller_id = $mkt->seller_id;
        $marketplace=Marketplace::where('_id',$marketplace_id)->first();
        $marketplace = $marketplace->marketplace_value;
        

        date_default_timezone_set('UTC');
        define ("AWS_ACCESS_KEY_ID", env("AWS_ACCESS_KEY_ID"));
        define ("MARKETPLACE_ID", $marketplace);
        define ("AWS_SECRET_ACCESS_KEY",env("AWS_SECRET_ACCESS_KEY"));
        define ("SELLER_ID",$seller_id);


        $params = array(
            'Query' => $query,
            'AWSAccessKeyId' => AWS_ACCESS_KEY_ID,
            'Action' => "ListMatchingProducts",
            'MWSAuthToken'=>$mws_token,
            'MarketplaceId'=> MARKETPLACE_ID,
            'SellerId'=> SELLER_ID,
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp'=> date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version'=> "2011-10-01");


        $url_parts = array();
        foreach(array_keys($params) as $key)
            $url_parts[] = $key . "=" . str_replace('%7E', '~', rawurlencode($params[$key]));
        sort($url_parts);

        $url_string = implode("&", $url_parts);
        $string_to_sign = "GET\nmws.amazonservices.com\n/Products/2011-10-01\n" . $url_string;


        $signature = hash_hmac("sha256", $string_to_sign, AWS_SECRET_ACCESS_KEY, TRUE);

        $signature = urlencode(base64_encode($signature));

        $url = "https://mws.amazonservices.com/Products/2011-10-01" . '?' . $url_string . "&Signature=" . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $response = curl_exec($ch);
        $plainXML = $this->mungXML($response);
        $arrayResult = (json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)));
        
        if ($query == trim($query) && strpos($query, ' ') !== false || ctype_lower($query)===true) {
            $products=(json_decode($arrayResult,true));

            return response()->json([
                'success' => true,
                'data' => $products
            ], 200);
        }

        else{
            $singleproduct=(json_decode($arrayResult,true));

            return response()->json([
                'success' => true,
                'data' => $singleproduct
            ], 200);
        }

    }

    public function getMyFeesEstimate(string $asin,string $shipping,string $amount){

        $user_id=auth()->user()->id;
        
        $mkt = SellerAccount::where('user_id', $user_id)->first();
        $marketplace_id = $mkt->marketplace_id;
        $mws_token = $mkt->mws_auth_token;
        $seller_id = $mkt->seller_id;
        // $marketplace=Marketplace::where('id',$marketplace_id)->first();
        $marketplace=$mkt->marketplace_id;
        define ("AWS_ACCESS_KEY_ID", env('AWS_ACCESS_KEY_ID'));
        define ("MARKETPLACE_ID", env('MARKETPLACE_ID'));
        define ("AWS_SECRET_ACCESS_KEY", env('AWS_SECRET_ACCESS_KEY'));
        $params = array(
            'AWSAccessKeyId' => AWS_ACCESS_KEY_ID,
            'Action' => "GetMyFeesEstimate",
            'MWSAuthToken'=>$mws_token,
            'MarketplaceId'=> MARKETPLACE_ID,
            'SellerId'=> $seller_id,
            'SignatureMethod' => "HmacSHA256",
            'SignatureVersion' => "2",
            'Timestamp'=> date("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time()),
            'Version'=> "2011-10-01",
            'FeesEstimateRequestList.FeesEstimateRequest.1.MarketplaceId' => $marketplace,
            'FeesEstimateRequestList.FeesEstimateRequest.1.IdType'=>"ASIN",
            'FeesEstimateRequestList.FeesEstimateRequest.1.IdValue'=>$asin,
            'FeesEstimateRequestList.FeesEstimateRequest.1.IsAmazonFulfilled'=>"true",
            'FeesEstimateRequestList.FeesEstimateRequest.1.Identifier'=>"request1",
            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.ListingPrice.Amount'=>$amount,
            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.ListingPrice.CurrencyCode'=>"USD",
            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.Shipping.Amount'=>$shipping,
            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.Shipping.CurrencyCode'=>"USD",
            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.Points.PointsNumber'=>"0",
            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.Points.PointsMonetaryValue.Amount'=>"0",
            'FeesEstimateRequestList.FeesEstimateRequest.1.PriceToEstimateFees.Points.PointsMonetaryValue.CurrencyCode'=>"USD"
            );


        $url_parts = array();
        foreach(array_keys($params) as $key)
            $url_parts[] = $key . "=" . str_replace('%7E', '~', rawurlencode($params[$key]));
        sort($url_parts);


        $url_string = implode("&", $url_parts);
        $string_to_sign = "POST\nmws.amazonservices.com\n/Products/2011-10-01\n" . $url_string;

        $signature = hash_hmac("sha256", $string_to_sign, AWS_SECRET_ACCESS_KEY, TRUE);

        $signature = urlencode(base64_encode($signature));

        $url = "https://mws.amazonservices.com/Products/2011-10-01" . '?' . $url_string . "&Signature=" . $signature;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://mws.amazonservices.com/Products/2011-10-01');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$url_string . "&Signature=" . $signature);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);


        $plainXML = $this->mungXML($response);
        $arrayResult = (json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)));
        $orders=(json_decode($arrayResult,true));
        
        return response()->json([
            'success' => true,
            'data' => $orders
        ], 200);
    }

    public function TestpushItem(Request $request){
    
        $user_id = auth()->user()->id;
        $mkt  =  SellerAccount::where('user_id', $user_id)->first();
        $marketplace_id  =  $mkt->marketplace_id;
        $mws_token  =  $mkt->mws_auth_token;
        $seller_id  =  $mkt->seller_id;

        $marketplace = Marketplace::where('_id',$marketplace_id)->first();
        $marketplace = $marketplace->marketplace_value;


        $asin = $request['asin'];
        $sku = PushedItem::max('id')+1;

        $price = $request['price'];
        $title = $request['title'];
        $manufacturer = $request['manufacturer'];

        $condition = $request['itemcond'];
        $xml_string = '<?xml version="1.0" encoding="iso-8859-1"?>
            <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
            <Header>
                <DocumentVersion>1.01</DocumentVersion>
                <MerchantIdentifier>'.$seller_id.'</MerchantIdentifier>
            </Header>
            <MessageType>Product</MessageType>
            <PurgeAndReplace>false</PurgeAndReplace>
            <Message>
                <MessageID>1</MessageID>
                <OperationType>Update</OperationType>
                <Product>
                <SKU>'.$sku.'</SKU>
                <StandardProductID>
                    <Type>ASIN</Type>
                    <Value>'.$asin.'</Value>
                </StandardProductID>
                <ProductTaxCode>A_GEN_NOTAX</ProductTaxCode>
                <Condition><ConditionType>'.$condition.'</ConditionType></Condition> 
                <DescriptionData>
                    <Title>'.$title.'</Title>
                    <Brand>'.$manufacturer.'</Brand>
                    <Description>Test Description</Description>
                    <BulletPoint>Point 1</BulletPoint>
                    <BulletPoint>Point 2</BulletPoint>
                    <MSRP currency="USD">'.$price.'</MSRP>
                    <Manufacturer>'.$manufacturer.'</Manufacturer>
                    <ItemType>example-item-type</ItemType>
                </DescriptionData>
                </Product>


            </Message>
            </AmazonEnvelope>';

        var_dump($xml_string);
        $var = base64_encode(md5(trim($xml_string),true));

        $param = array();
        $param['AWSAccessKeyId']   = 'AKIAJ3QLKPQ3SNMG743Q';
        $param['Action']           = 'SubmitFeed';
        $param['FeedType']         = '_POST_PRODUCT_DATA_';
        $param['MWSAuthToken']     = 'amzn.mws.80a4f17a-b77f-8222-4115-5858a7d46460';
        $param['MarketplaceId.Id'] = 'ATVPDKIKX0DER';
        $param['SellerId']         = 'A17JA29WLFZC5I';
        $param['ContentMD5Value']  = $var;
        $param['FeedContent']      = trim($xml_string);
        $param['SignatureMethod']  = 'HmacSHA256';
        $param['SignatureVersion'] = '2';
        $param['Timestamp'] = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
        $param['PurgeAndReplace']  = 'false';
        $param['Version']          = '2009-01-01';


        $secret = '/UCCJg3bi/k1D1cJHKM4iMHFxjGuAKh5DrGy++3X';

        $url = array();
        foreach ($param as $key => $val) {

            $key = str_replace("%7E", "~", rawurlencode($key));
            $val = str_replace("%7E", "~", rawurlencode($val));
            $url[] = "{$key}={$val}";
        }

        sort($url);

        $arr   = implode('&', $url);

        $sign  = 'POST' . "\n";
        $sign .= 'mws.amazonservices.com' . "\n";
        $sign .= '/Feeds/2009-01-01' . "\n";
        $sign .= $arr;

        $signature = hash_hmac("sha256", $sign, $secret, true);
        $signature = urlencode(base64_encode($signature));

        $link  = "https://mws.amazonservices.com/Feeds/2009-01-01?";
        $link .= $arr . "&Signature=" . $signature;
        echo($link); //for debugging - you can paste this into a browser and see if it loads.

        $headers=array(
            'Content-Type: application/xml',
            'Content-Length: ' . strlen($xml_string),
            'Content-MD5: ' . $var
        );


        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        var_dump($response);

        sleep(30);


        $xml_string='<?xml version="1.0"?>
            <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
                <Header>
                    <DocumentVersion>1.01</DocumentVersion>
                    <MerchantIdentifier>'.$seller_id.'</MerchantIdentifier>
                </Header>
                <MessageType>Inventory</MessageType>
                <Message>
                    <MessageID>1</MessageID>
                    <OperationType>Update</OperationType>
                    <Inventory>
                        <SKU>'.$sku.'</SKU>
                        <Quantity>5</Quantity>
                        <FulfillmentLatency>1</FulfillmentLatency>
                    </Inventory>
                </Message>
            </AmazonEnvelope>';

        $var = base64_encode(md5(trim($xml_string),true));

        $param = array();
        $param['AWSAccessKeyId']   = 'AKIAJ3QLKPQ3SNMG743Q';
        $param['Action']           = 'SubmitFeed';
        $param['FeedType']         = '_POST_INVENTORY_AVAILABILITY_DATA_';
        $param['MWSAuthToken']     = 'amzn.mws.80a4f17a-b77f-8222-4115-5858a7d46460';
        $param['MarketplaceId.Id'] = 'ATVPDKIKX0DER';
        $param['SellerId']         = 'A17JA29WLFZC5I';
        $param['ContentMD5Value']  = $var;
        $param['FeedContent']      = trim($xml_string);
        $param['SignatureMethod']  = 'HmacSHA256';
        $param['SignatureVersion'] = '2';
        $param['Timestamp'] = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
        $param['PurgeAndReplace']  = 'false';
        $param['Version']          = '2009-01-01';


        $secret = '/UCCJg3bi/k1D1cJHKM4iMHFxjGuAKh5DrGy++3X';

        $url = array();
        foreach ($param as $key => $val) {

            $key = str_replace("%7E", "~", rawurlencode($key));
            $val = str_replace("%7E", "~", rawurlencode($val));
            $url[] = "{$key}={$val}";
        }

        sort($url);

        $arr   = implode('&', $url);

        $sign  = 'POST' . "\n";
        $sign .= 'mws.amazonservices.com' . "\n";
        $sign .= '/Feeds/2009-01-01' . "\n";
        $sign .= $arr;

        $signature = hash_hmac("sha256", $sign, $secret, true);
        $signature = urlencode(base64_encode($signature));

        $link  = "https://mws.amazonservices.com/Feeds/2009-01-01?";
        $link .= $arr . "&Signature=" . $signature;
        echo($link); //for debugging - you can paste this into a browser and see if it loads.

        $headers=array(
            'Content-Type: application/xml',
            'Content-Length: ' . strlen($xml_string),
            'Content-MD5: ' . $var
        );


        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        var_dump($response);



        sleep(30);


        $xml_string='<?xml version="1.0"?>
            <AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
                <Header>
                    <DocumentVersion>1.01</DocumentVersion>
                    <MerchantIdentifier>'.$seller_id.'</MerchantIdentifier>
                </Header>
                <MessageType>Price</MessageType>
                <Message>
                    <MessageID>1</MessageID>
                    <OperationType>Update</OperationType>
                <Price>
                <SKU>'.$sku.'</SKU>
                <StandardPrice currency="USD">'.$price.'</StandardPrice>
                </Price>
                </Message>
            </AmazonEnvelope>';

        $var = base64_encode(md5(trim($xml_string),true));

        $param = array();
        $param['AWSAccessKeyId']   = 'AKIAJ3QLKPQ3SNMG743Q';
        $param['Action']           = 'SubmitFeed';
        $param['FeedType']         = '_POST_PRODUCT_PRICING_DATA_';
        $param['MWSAuthToken']     = 'amzn.mws.80a4f17a-b77f-8222-4115-5858a7d46460';
        $param['MarketplaceId.Id'] = 'ATVPDKIKX0DER';
        $param['SellerId']         = 'A17JA29WLFZC5I';
        $param['ContentMD5Value']  = $var;
        $param['FeedContent']      = trim($xml_string);
        $param['SignatureMethod']  = 'HmacSHA256';
        $param['SignatureVersion'] = '2';
        $param['Timestamp'] = gmdate("Y-m-d\TH:i:s.\\0\\0\\0\\Z", time());
        $param['PurgeAndReplace']  = 'false';
        $param['Version']          = '2009-01-01';


        $secret = '/UCCJg3bi/k1D1cJHKM4iMHFxjGuAKh5DrGy++3X';

        $url = array();
        foreach ($param as $key => $val) {

            $key = str_replace("%7E", "~", rawurlencode($key));
            $val = str_replace("%7E", "~", rawurlencode($val));
            $url[] = "{$key}={$val}";
        }

        sort($url);

        $arr   = implode('&', $url);

        $sign  = 'POST' . "\n";
        $sign .= 'mws.amazonservices.com' . "\n";
        $sign .= '/Feeds/2009-01-01' . "\n";
        $sign .= $arr;

        $signature = hash_hmac("sha256", $sign, $secret, true);
        $signature = urlencode(base64_encode($signature));

        $link  = "https://mws.amazonservices.com/Feeds/2009-01-01?";
        $link .= $arr . "&Signature=" . $signature;
        echo($link); //for debugging - you can paste this into a browser and see if it loads.

        $headers=array(
            'Content-Type: application/xml',
            'Content-Length: ' . strlen($xml_string),
            'Content-MD5: ' . $var
        );


        $ch = curl_init($link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        var_dump($response);
        $sku=$sku+1;
        PushedItem::create(['id'=>$sku]);

    }
}
