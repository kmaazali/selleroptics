<?php
namespace App\Http\Controllers;
use App\User;
use App\Log;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Mail\VerifyAccount;
use App\Mail\ResetPassword;
use App\Mail\BlockUser;
use App\Mail\BlockEmployee;
use Auth;
use App;
use View;
use Illuminate\Support\Facades\Input;

use   Tymon\JWTAuth\PayloadFactory;
use   Tymon\JWTAuth\JWTManager as JWT;

class UserController extends Controller
{

    public function register(Request $request){

        $validator = Validator::make($request->json()->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
            
        $user = User::create([
            'name' => $request->json()->get('name'),
            'email' => $request->json()->get('email'),
            'password' => Hash::make($request->json()->get('password')),
            'phone' =>$request->json()->get('phone'),
            'role_id'=>2,
            'company_name' =>$request->json()->get('company_name'),
            'is_signup_complete'=>true
            ]);
                
        $token = JWTAuth::fromUser($user);
                
        $user=User::where('email',$request->json()->get('email'))->first();

        $data = array(
            'name' => $user->name,
            'id' =>  $user->id,
            'email'=>$user->email,
        );

        $when = Carbon::now()->addSeconds(5);
        Mail::to($data['email'])->later($when,new VerifyAccount($data));

        return response()->json(['user'=>$user,'data'=>$data, 'token'=>$token,'success'=>true,'message'=>'Account Created Successfully'],201);
    }

    public function forgetPassword(Request $request){

        $user = User::where('email',$request['email'])->first();

        $data = array(
            'name' => $user->name,
            'id' =>  $user->id,
            'email'=>$user->email
        );

        $when = Carbon::now()->addSeconds(5);

        Mail::to($data['email'])->later($when,new ResetPassword($data));

        return View::make('forgot')->with($data);
    }
    
    public function login(Request $request){

        $credentials = $request->json()->all();

        $user = User::where('email',$credentials['email'])->first();

            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    $user->attempt += 1;
                    $user->save();
                    if ($user->attempt >= 4 && $user->role_id != 1 && $user->role_id == 2) {
                        
                        $user->attempt_at = Carbon::now()->addMinutes(2)->toDateTimeString();
                        $user->is_blocked = true;
                        $user->save();
                        $super = User::where('role_id', 1)->get();
                        // dd($super);
                        foreach ($super as $admin) {
                        
                        $data = array(
                            'name' => $admin->name,
                            'email' => $admin->email
                        );

                        $when = Carbon::now()->addSeconds(5);

                        Mail::to($data['email'])->later($when,new BlockUser($data));

                        }
                    }

                    if ($user->attempt >= 4 && $user->role_id != 1 && $user->role_id == 3) {
                        
                        $user->attempt_at = Carbon::now()->addMinutes(2)->toDateTimeString();
                        $user->is_blocked = true;
                        $user->save();
                        
                        $client = $user=User::where('_id',$user->client_id)->first();
                        $data = array(
                            'client_name' => $client->name,
                            'employee_name' => $user->name,
                            'email' => $client->email
                        );

                        $when = Carbon::now()->addSeconds(5);

                        Mail::to($data['email'])->later($when,new BlockEmployee($data));
                    }

                    return response()->json(['error' => 'invalid_credentials'], 400);
                }
            } 
            catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

            if($user->is_signup_complete == true){

            $start = new Carbon(Carbon::now()->toDateTimeString());
            $end = new Carbon($user->attempt_at);
    
            if ($start->gt($end) == true) {
                $user->is_blocked = false;
                $user->attempt = 0;
                $user->attempt_at = '';
                $user->save();
            }
            $email=$request->json()->get('email');
            $user=User::where('email',$email)->first();
            $req = $request->json()->all()['email'];

            $string=exec('getmac');
            $mac=substr($string, 0, 17); 
            // dd($mac);
            $this->checkMac($email, $user->_id, $user->role_id);
            $this->log($req, $mac);
    
            return response()->json(['success'=>true,'token'=>$token,'user'=>$user],200);
        }
    }

    public function getAuthenticatedUser(){
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json(['token_expired'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json(['token_invalid'], $e->getStatusCode());
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_absent'], $e->getStatusCode());
        }
        return response()->json(compact('user'));
    }

    public function logout(){

        JWTAuth::invalidate();
        return response([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }

    public function sendWelcomeEmail(){
        $currentUser = Auth::user();
        $data = array(
            'user' => $currentUser
        );

        $user = array(
            'name' => $data['user']['name'],
            'id' =>  $data['user']['id'],
            'email'=>$data['user']['email'],
        );
        // sending email for password recovery
        $when = Carbon::now()->addSeconds(5);
        Mail::to($data['user']['email'])->later($when,new VerifyAccount($user));

        return response()->json(['success'=>true,'message'=>'Email Resent Successfully']);
    }


    public function verifyAccount($id){

        $user=User::find($id);
        $user->is_email_confirmed=true;
        $user->save();
        echo "<script>window.close();</script>";
    }

    public function resetPassword(Request $request){

        $referer = $_SERVER['HTTP_REFERER'];
        $id = explode('=', $referer);
        $user = User::where('_id',$id[1])->first();
        $user->password = Hash::make($request['password']);
        $user->save();
        $url = App::make('url')->to('/');

        return redirect('/');
    }

    public function log($email, $mac){

        $user = User::where('email',$email)->first();
        
        $log = Log::create([
            'user_id' => $user->_id,
            'name' => $user->name,
            'role_id' => $user->role_id,
            'email' => $email,
            'mac' => $mac
        ]);

        return $log;
    }

    public function checkMac($email, $id, $role_id){

        $log = Log::where('email', $email)->distinct('mac')->get()->count();
        
        if($log >= 5 && $role_id != 1){
            $admin = new AdminController();
            $admin->blockUser($id);
        }
    }

    public function addEmployee(Request $request){

        $client_id = $request['client_id'];
        $validator = Validator::make($request->json()->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $limit = User::where('client_id',$request->client_id)->count();
        if($limit < 5){

            $user = User::create([
                'name' => $request->json()->get('name'),
                'email' => $request->json()->get('email'),
                'password' => Hash::make($request->json()->get('password')),
                'phone' =>$request->json()->get('phone'),
                'role_id'=>3,
                'client_id'=>$client_id,
                'company_name' =>$request->json()->get('company_name'),
                'is_signup_complete'=>false,
            ]);

            $token = JWTAuth::fromUser($user);

            $user=User::where('email',$request->json()->get('email'))->first();

            $data = array(
                'name' => $user->name,
                'id' =>  $user->id,
                'email'=>$user->email,
            );

            return response()->json(['user'=>$user,'data'=>$data, 'token'=>$token,'success'=>true,'message'=>'Account Created Successfully'],201);
        }
        else{
            return response()->json('Limit Exceeded');
        }
    }
}
