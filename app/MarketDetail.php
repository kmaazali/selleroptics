<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MarketDetail extends Eloquent
{
    protected $table='market_details';

    protected $fillable=['user_id'];
}
