<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Log extends Eloquent
{
    protected $table='logs';

    protected $fillable = [
        'user_id', 'role_id', 'name','email','mac'
    ];
}
