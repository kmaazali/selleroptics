<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Pricer extends Eloquent
{
    protected $table='pricers';

    protected $fillable=['list_price_value','list_price_unit','user_id','is_pricer'];
}
