<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

trait CommonTrait {

    // get last id in a collection
    public function get_last_id($collection_name, $field_name) {
        $collection = app("App\\$collection_name");
        $records = $collection::all();
        if (count($records) > 0) {
            $last_id = $collection ::orderBy($field_name, 'desc')->take(1)->get();
            $incremental_id = $last_id[0][$field_name] + 1;
        } else {
            $incremental_id = 1;
        }
        return $incremental_id;
    }

    //   pagination on arary results
    public function array_pagination($responseData, $total, $perPage, $page){

        $pagination =   new \Illuminate\Pagination\LengthAwarePaginator($responseData, $total, $perPage, $page, [
            'path' => \Illuminate\Pagination\Paginator::resolveCurrentPath(),
        ]);
        $array =    json_encode($pagination);
        $new_array = json_decode( $array);
        unset($new_array->first_page_url);
        unset($new_array->last_page_url);
        unset($new_array->next_page_url);
        unset($new_array->path);
        unset($new_array->prev_page_url);

        return $new_array;
    }

    public function mungXML($xml){

        $obj = SimpleXML_Load_String($xml);
        if ($obj === FALSE) return $xml;

        // Capturing Namespaces If There Are Any
        $nss = $obj->getNamespaces(TRUE);
        if (empty($nss)) return $xml;

        // Change ns: into ns_
        $nsm = array_keys($nss);
        foreach ($nsm as $key)
        {
            // A REGULAR EXPRESSION TO MUNG THE XML
            $rgx
                = '#'
                . '('
                . '\<'
                . '/?'
                . preg_quote($key)
                . ')'
                . '('
                . ':{1}'
                . ')'
                . '#'
            ;

            $rep
                = '$1'
                . '_'
            ;
            // Execute the replacement with the regular expressions listed
            $xml =  preg_replace($rgx, $rep, $xml);
        }
        return $xml;
    }

    public function getClientIps()
    {
        $clientIps = array();
        $ip = $this->server->get('REMOTE_ADDR');
        if (!$this->isFromTrustedProxy()) {
            return array($ip);
        }
        if (self::$trustedHeaders[self::HEADER_FORWARDED] && $this->headers->has(self::$trustedHeaders[self::HEADER_FORWARDED])) {
            $forwardedHeader = $this->headers->get(self::$trustedHeaders[self::HEADER_FORWARDED]);
            preg_match_all('{(for)=("?\[?)([a-z0-9\.:_\-/]*)}', $forwardedHeader, $matches);
            $clientIps = $matches[3];
        } elseif (self::$trustedHeaders[self::HEADER_CLIENT_IP] && $this->headers->has(self::$trustedHeaders[self::HEADER_CLIENT_IP])) {
            $clientIps = array_map('trim', explode(',', $this->headers->get(self::$trustedHeaders[self::HEADER_CLIENT_IP])));
        }
        $clientIps[] = $ip; // Complete the IP chain with the IP the request actually came from
        $ip = $clientIps[0]; // Fallback to this when the client IP falls into the range of trusted proxies
        foreach ($clientIps as $key => $clientIp) {
            // Remove port (unfortunately, it does happen)
            if (preg_match('{((?:\d+\.){3}\d+)\:\d+}', $clientIp, $match)) {
                $clientIps[$key] = $clientIp = $match[1];
            }
            if (IpUtils::checkIp($clientIp, self::$trustedProxies)) {
                unset($clientIps[$key]);
            }
        }
        // Now the IP chain contains only untrusted proxies and the client IP
        return $clientIps ? array_reverse($clientIps) : array($ip);
    } 
}
