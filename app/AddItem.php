<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class AddItem extends Eloquent
{
    protected $table='add_items';

    protected $fillable=['user_id','asin','list_price','category','image','title','rank','my_price','quantity','msku','condition','note','tax_code','my_cost','fees','profit_loss'];

}
