<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class PaymentUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily Payment Check';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
              // $payment = new \App\Payment();
        $dayAgo = 14;
        $dayToCheck = Carbon::now()->subDays($dayAgo);
        $pak = \App\Payment::where("created_at", '=', $dayToCheck)->get();
        foreach ($pak as $paks) {
            // echo $paks;
            $user_id = $paks->external_customer_id;
            $card_id = $paks->credit_card_id;
            $amount = '50';

            $ch = curl_init();
            $data = '{
            "user_id": "'.$user_id.'",
            "card_id": "'.$card_id.'",
            "amount": "'.$amount.'"
            }';
            
            curl_setopt($ch, CURLOPT_URL, "http://127.0.0.1:8000/api/payment/creditcard/curl");
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSLVERSION , 6); //NEW ADDITION
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            // curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "{$data}");

            // Set HTTP Header for POST request 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type:application/json"
                )
            );

            $result = curl_exec($ch);
            // dd($result);
            
            
            curl_close($ch);
        }
        
        $this->info('Daily payment checked....');
    }
}
