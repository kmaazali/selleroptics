<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class GradingNote extends Eloquent
{
    protected $table='grading_notes';

    protected $fillable=['user_id','fulfillment','condition','subcondition','category','note'];
}
