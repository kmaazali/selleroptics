<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Payment extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'payments';

    protected $fillable = [
        'credit_card_id', 'payment_id', 'external_customer_id', 'package_id'
    ];
}
