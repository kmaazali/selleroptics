<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Addresses extends Eloquent
{
    protected $table='addresses';

    protected $fillable=['name','address_line_1','address_line_2','city','zip_code','country','state','user_id'];
}
