<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Marketplace extends Eloquent
{
    protected $table='marketplaces';

    protected $fillable=['marketplace_id','marketplace_name','marketplace_value'];
}
