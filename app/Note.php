<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Note extends Eloquent
{
    protected $table='notes';

    protected $fillable=['user_id','category','condition','note'];
}
