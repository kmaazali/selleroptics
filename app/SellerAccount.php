<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SellerAccount extends Eloquent
{
    protected $table='seller_accounts';

    protected $fillable=['user_id'];
}
