<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PushedItem extends Eloquent
{
    protected $table='pushed_items';

    protected $fillable=['sku'];
}
