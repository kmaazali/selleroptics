
      $(document).ready(function(){
      $('.customer-logos').slick({
        slidesToShow: 14,
        slidesToScroll: 1,
        autoplay:true ,
        autoplaySpeed: 1500,
        arrows: true,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
      });
      });


      $(document).ready(function(){
        $('.form-handle').click(function(){
          $('.sideform').toggleClass('show_form');
        });
      });

$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    
    
});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

  $('.button.right').on('click', function() {
  $('.button-group').removeClass('left');
  $('.button-group').addClass('right');
  $('.button').removeClass('active');
  $(this).addClass('active');
});

$('.button.left').on('click', function() {
  $('.button-group').removeClass('right');
  $('.button-group').addClass('left');
  $('.button').removeClass('active');
  $(this).addClass('active');
});

$(document).ready(function(){
  $(".wellcomeEmail input").focus( function () {
    $('.wellcomeEmail span').removeClass("active");
    $(this).parent().find("span").addClass("active");
  });
  $(".wellcomeEmail input").focusout( function () {
    $('.wellcomeEmail span').removeClass("active");
  });
  });

 function onChangeCallback(ctr){
            console.log("The country was changed: " + ctr);
            //$("#selectionSpan").text(ctr);
        }

        $(document).ready(function () {
            $(".niceCountryInputSelector").each(function(i,e){
                new NiceCountryInput(e).init();
  });
});


 $(document).ready(function() {
 $('.se-pre-con').fadeOut('show');
  setTimeout(function() {
    $('.se-pre-con').fadeOut('hide');
}, 1000);
  });



$(function() {
  $('#colorselector').change(function(){
    $('.colors').hide();
    $('#' + $(this).val()).show();
  });
});

  $(function(){
    $('#sendResetLink').on('submit', function(event){
      $('.error').removeClass('error');
      $('.form-validation-message').css("display", "none");
      var re = /\S+@\S+\.\S+/ ;
      let email= $('[name=usr_email]').val();     
      if(!email || !$.trim(email)){
        $('[name=usr_email]').addClass('error');
        $('.form-validation-message').html(`
                        <img src="../../assets/img/warn_ico.svg" 
                          style="width:40px; height:27px; margin:0px -4px;">
                        </img>
                        Email address is required
                      `);           
        $('.form-validation-message').css("display", "block");              
      }
      else if(!re.test(email)){
        $('[name=usr_email]').addClass('error');  
        $('.form-validation-message').html(`
                        <img src="../../assets/img/warn_ico.svg" 
                          style="width:40px; height:27px; margin:0px -4px;">
                        </img>
                        Please enter valid email address 
                      `);
        $('.form-validation-message').css("display", "block");          
      }       
      event.preventDefault(); 
    });
  });

  $('a.nav-link').click(function(){
$('a.nav-link').removeClass('collapsed');
$(this).addClass('collapsed');
$('.collapse').removeClass('show');
$('a.nav-link').attr('aria-expanded',false);
$(this).attr('aria-expanded',false);

 });


$(".modal").click(function() {
          $('.itemmodal').modal('show');
          $('.itemmodal').modal('hide');
       });
$(".modal").click(function() {
          $('.labelmodal').modal('show');
          $('.labelmodal').modal('hide');
       });
    $(document).ready(function() {
    $('.button1').on('click', function() {
        $('.section_one').hide();
        $('.section_two').show();
        $('.button1').hide();
        $('.button2').show();
    });
    
    $('.button2').on('click', function() {
        $('.section_one').show();
        $('.section_two').hide();
         $('.button1').show();
         $('.button2').hide();
    });    
});
  $(".download-icon-printer").click(function(){
  $(".printBoxs").toggleClass("active").focus;
  $(".printBoxs").val("");
});
new WOW().init();
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}


