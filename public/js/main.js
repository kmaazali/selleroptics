
/******** LOGIN SCREEN JS *******/

$(document).ready(function(){
    $(".inner-addon input").focus( function () {
        $('.inner-addon span').removeClass("active");
        $(this).parent().find("span").addClass("active");
    });
    $(".inner-addon input").focusout( function () {
        $('.inner-addon span').removeClass("active");
    });
});


$(function(){
    $('#doLogin').on('submit', function(event){
        $('.error').removeClass('error');
        $('.form-validation-message').css("display", "none");
        var re = /\S+@\S+\.\S+/ ;
        let email= $('[name=usr_email]').val();
        let pwd = $('[name=usr_pwd]').val();
        if(!email || !$.trim(email)){
            $('[name=usr_email]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Email address is required
											`);
            $('.form-validation-message').css("display", "block");
        }
        else if(!pwd){
            $('[name=usr_pwd]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												User password is required
											`);
            $('.form-validation-message').css("display", "block");
        }
        else if(!re.test(email)){
            $('[name=usr_email]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Please enter valid email address 
											`);
            $('.form-validation-message').css("display", "block");
        }
        event.preventDefault();
    });
});





/************* Sign Up Screen*************/

$("#Agree").on('click', function(){
    let state = parseInt($(this).attr("state"))? '0': '1';
    $(this).attr("state",state.toString());
    $(this).toggleClass('active');
});

$(function(){
    $('#doSignUp').on('submit', function(event){
        $('.error').removeClass('error');
        $('.form-validation-message').css("display", "none");
        var re = /\S+@\S+\.\S+/ ;
        var CapitalLetters = /[A-Z]/;
        var Numbers = /[0-9]/;
        let name= $('[name=usr_name]').val();
        let company= $('[name=company_name]').val();
        let email= $('[name=usr_email]').val();
        let phone= $('[name=usr_phone]').val();
        let userpwd = $('[name=usr_pwd]').val();
        let cnfrimpwd = $('[name=cnfm_pwd]').val();
        let acceptTerms = $("#Agree").attr("state");

        if(!name || !$.trim(name)){
            $('[name=usr_name]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												User name is required
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(!email || !$.trim(email)){
            $('[name=usr_email]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Email address is required
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(!re.test(email)){
            $('[name=usr_email]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Please enter valid email address 
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(!phone || !$.trim(phone)){
            $('[name=usr_phone]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Phone Number is required
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(!userpwd || !$.trim(userpwd)){
            $('[name=usr_pwd]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												User password is required
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(!CapitalLetters.test(userpwd) || !Numbers.test(userpwd) || userpwd.length < 8){
            $('[name=usr_pwd]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Password should be 8 characters long & must contain atleast one capital alphabet and one number
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(!cnfrimpwd || !$.trim(cnfrimpwd)){
            $('[name=cnfm_pwd]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Please confirm password
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(cnfrimpwd!==userpwd){
            $('[name=cnfm_pwd]').addClass('error');
            $('[name=usr_pwd]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												User password does not match confirmed password
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(acceptTerms=='0'){
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												You must agree to our terms of services to continue
											`);
            $('.form-validation-message').css("display", "block");
        }

        event.preventDefault();
    });
});

/************* Forgot Password Screen*************/

$(function(){
    $('#sendResetLink').on('submit', function(event){
        $('.error').removeClass('error');
        $('.form-validation-message').css("display", "none");
        var re = /\S+@\S+\.\S+/ ;
        let email= $('[name=usr_email]').val();
        if(!email || !$.trim(email)){
            $('[name=usr_email]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Email address is required
											`);
            $('.form-validation-message').css("display", "block");
        }
        else if(!re.test(email)){
            $('[name=usr_email]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Please enter valid email address 
											`);
            $('.form-validation-message').css("display", "block");
        }
        event.preventDefault();
    });
});


/************* Verify Questions Screen*************/

$(function(){
    $('#verifyQuestions').on('submit', function(event){
        $('.error').removeClass('error');
        $('.form-validation-message').css("display", "none");
        let high_school= $('[name=high_school]').val();
        let hometown = $('[name=hometown]').val();
        if(!high_school || !$.trim(high_school)){
            $('[name=high_school]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Please answer verification questions to continue
											`);
            $('.form-validation-message').css("display", "block");
        }
        else if(!hometown || !$.trim(hometown)){
            $('[name=hometown]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Please answer verification questions to continue
											`);
            $('.form-validation-message').css("display", "block");
        }
        event.preventDefault();
    });
});


/************* Reset Password Screen*************/

$(function(){
    $('#passwordReset').on('submit', function(event){
        $('.error').removeClass('error');
        $('.form-validation-message').css("display", "none");
        var CapitalLetters = /[A-Z]/;
        var Numbers = /[0-9]/;
        let userpwd = $('[name=usr_pwd]').val();
        let cnfrimpwd = $('[name=cnfm_pwd]').val();
        if(!userpwd || !$.trim(userpwd)){
            $('[name=usr_pwd]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												User password is required
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(!CapitalLetters.test(userpwd) || !Numbers.test(userpwd) || userpwd.length < 8){
            $('[name=usr_pwd]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Password should be 8 characters long & must contain atleast one capital alphabet and one number
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(!cnfrimpwd || !$.trim(cnfrimpwd)){
            $('[name=cnfm_pwd]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Please confirm password
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(cnfrimpwd!==userpwd){
            $('[name=cnfm_pwd]').addClass('error');
            $('[name=usr_pwd]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												User password does not match confirmed password
											`);
            $('.form-validation-message').css("display", "block");
        }
        event.preventDefault();
    });
});


/************* MWS Auth Screen *************/

$('.custom-country-select').click(function(){
    $(this).addClass('focus');
});


$('#collapseMenu').on('hidden.bs.collapse', function (e) {
    $('.custom-country-select').removeClass('focus');
});

$(function(){
    $('#Auth').on('submit', function(event){
        let seller_id = $('[name=seller_id]').val();
        let auth_token = $('[name=auth_token]').val();
        let selected_country = $('#country-select').attr('value');
        $('.error').removeClass('error');
        $('.form-validation-message').css("display", "none");
        if(!$.trim(selected_country)){
            $('#country-select').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Please select marketplace first
											`);
            $('.form-validation-message').css("display", "block");
        }
        else if(!seller_id || !$.trim(seller_id)){
            $('[name=seller_id]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Please enter seller ID
											`);
            $('.form-validation-message').css("display", "block");
        }

        else if(!auth_token || !$.trim(auth_token)){
            $('[name=auth_token]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Please enter authorization token
											`);
            $('.form-validation-message').css("display", "block");
        }
        event.preventDefault();
    });
});


/************* Select Plan Table Screen *************/

$('#SelectBasic').on('click', function(){
    $('#SelectBasic').html(`<div style="display:flex; color:#8ca6b4;">
											<span>Selected</span>  
											<img class="select-check" src="/utils/icons/circle_check_ico.svg" alt="✓"></img>
									</div>`);
    $('#SelectStandard').html(`<span style="color:#c5892f;">Select</span>`);
    document.getElementById('SelectPlanTable').setAttribute("selected", "basic");
    $('.check-mark-plan').removeClass('check-mark-plan');
    $("#basic-checked").addClass('check-mark-plan');
});

$('#SelectStandard').on('click', function(){
    $('#SelectStandard').html(`<div style="display:flex; color:#8ca6b4;">
											<span>Selected</span>  
											<img class="select-check" src="/utils/icons/circle_check_ico.svg" alt="✓"></img>
									</div>`);
    $('#SelectBasic').html(`<span style="color:#c5892f;">Select</span>`);
    document.getElementById('SelectPlanTable').setAttribute("selected", "standard");
    $('.check-mark-plan').removeClass('check-mark-plan');
    $("#standard-checked").addClass('check-mark-plan');
});

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})


$('[name=expire_date]').on('click',function(){
    $(this).attr('type','date').addClass('custom-login-date-field');
});

$('[name=expire_date]').change(function(){
    $(this).blur().attr('type','text').removeClass('custom-login-date-field');
    let ExpireDate = $('[name=expire_date]').val();
    $(this).val(ExpireDate.split('-')[0]+'/'+ExpireDate.split('-')[1]);
});

$(function(){
    $('#plan').on('submit', function(event){
        $('.error').removeClass('error');
        $('.form-validation-message').css("display", "none");
        let selected_plan = $("#SelectPlanTable").attr('selected');
        let cardHolder= $('[name=card_holder_name]').val();
        let cardNumber= $('[name=card_number]').val();
        let ExpireDate = $('[name=expire_date]').val();
        let cvc= $('[name=cvc]').val();
        if(!cardHolder || !$.trim(cardHolder)){
            $('[name=card_holder_name]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Card holder name is required
											`);
            $('.form-validation-message').css("display", "block");
        }
        else if(!cardNumber || !$.trim(cardNumber)){
            $('[name=card_number]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Card Number is required
											`);
            $('.form-validation-message').css("display", "block");
        }
        else if(!ExpireDate || !$.trim(ExpireDate)){
            $('[name=expire_date]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												Expire date is required
											`);
            $('.form-validation-message').css("display", "block");
        }
        else if(!cvc || !$.trim(cvc)){
            $('[name=cvc]').addClass('error');
            $('.form-validation-message').html(`
												<img src="/utils/icons/warn_ico.svg" 
													style="width:40px; height:27px; margin:0px -4px;">
												</img>
												CVC field is required
											`);
            $('.form-validation-message').css("display", "block");
        }
        event.preventDefault();
    });
});

