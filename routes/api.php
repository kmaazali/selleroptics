<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Auth::routes();
Route::post('/auth/register','UserController@register');
Route::post('/auth/login','UserController@login');
Route::get('/verifyaccount/{id}','UserController@verifyAccount');

Route::post('/payment/creditcard/curl','PaymentController@vaultPaymentCurl');



Route::group(['middleware' => 'auth.jwt'], function () {


    Route::group(['prefix'=>'admin', 'middleware'=>'superadmin'],function(){
        Route::get('/users/block/{id}','AdminController@blockUser');
        Route::get('/users/unblock/{id}','AdminController@unblockUser');
        Route::post('/admin/register/employee','AdminController@adminAddEmployee');
        Route::get('/users','AdminController@getUsers');
        Route::get('/users/dropdown','AdminController@getUsersDropdown');
        Route::post('/reset/password','AdminController@resetPassword');
        Route::post('/users/delete','AdminController@deleteUser');
    });
    Route::group(['prefix'=>'client', 'middleware'=>'client'],function(){
        Route::post('/register/employee','UserController@addEmployee');
        //APIController Routes:
        //Params required: each_list_price, flat_price_value
        Route::post('/updatepricers','ApiController@updatePricers');

        Route::get('/getpricers/{id}','ApiController@getPricers');

        // Route::get('get/estimate','AmazonController@getMyFeesEstimate');

        //View all addresses
        Route::get('/settings/addresses','ApiController@getAddresses');

        //Add new address
        Route::post('/add/address','ApiController@addAddress');

        //Delete address
        Route::delete('/delete/address/{id}','ApiController@deleteAddress');

        //Edit address
        Route::put('/edit/address/{id}','ApiController@editAddress');

        //Edit with post
        Route::post('/edit/address','ApiController@postEditAddress');

        Route::post('/gradeitem','ApiController@gradeItem');
        //Params required: seller_id, mws_auth, marketplace
        Route::post('/select/marketplace','ApiController@selectMarketplace');

        //AmazonController Routes:
        //Params required: query
        Route::post('/searchproduct','AmazonController@searchProduct');

        //Params required: asin->ASIN, ISBN or Any Product Identifier
        Route::post('/comparicingforasin','AmazonController@getLowestPricedOfferings');

        //Params required: asin->ASIN, ISBN or Any Product Identifier
        Route::post('/lowestofferingforasin','AmazonController@lowestOfferForAsin');

        //Params required: asin->ASIN, ISBN or Any Product Identifier
        Route::post('/getcompetitiveprice','AmazonController@getCompetitivePricing');

        //Params required: datevalue, endpoint
        Route::post('/search/orders','AmazonController@listOrders');

        //Params required: NextToken, datevalue, endpoint
        Route::post('/orderlistnexttoken','AmazonController@orderListNextToken');

        //Params required: query->ASIN, ISBN or Any Product Identifier
        Route::post('/search/single/asin','AmazonController@searchsingleasin');

        //Params required: query->Any product name
        Route::post('/finalsearch/submit','AmazonController@ListMatchingProduct');

        // Route::post('getoffers/asin','ApiController@getLowestPricedOfferings');


        Route::get('getestimate/{asin}/{shipping}/{amount}','AmazonController@getMyFeesEstimate');

        Route::post('/push/product','AmazonController@TestpushItem');

        Route::post('/payment/vault/creditcard','PaymentController@vaultCreditCard');
        
        Route::post('/payment/creditcard','PaymentController@vaultPayment');

        Route::get('/set/packages/{id}','ApiController@setPackages');

        Route::post('/add/note', 'ApiController@addNote');

        Route::get('/get/note/{condition}', 'ApiController@getNotes');

        Route::get('/get/grading/note/{condition}', 'ApiController@getGradingNotes');

        Route::get('/get/all/categories', 'ApiController@getAllCategories');

        Route::post('/add/item', 'ApiController@addItem');

        Route::post('/post/note', 'ApiController@postNotes');

        Route::get('/fetch/note/{condition}', 'ApiController@fetchNotes');
        
    });
    
    Route::get('/sendwelcomemail','UserController@sendWelcomeEmail');
    Route::get('/logout', 'UserController@logout');

    // Route::get('/auth/user', 'UserController@getAuthenticatedUser');

});

