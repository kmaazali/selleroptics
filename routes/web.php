<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('forgot', function () {
    return view('forgot');
});
Route::post('/forgetpassword','UserController@forgetPassword');
Route::get('reset-password', function () {
    return view('reset');
});
Route::post('/resetpassword','UserController@resetPassword');

Route::get('auth/register','UserController@register123');
Auth::routes();

