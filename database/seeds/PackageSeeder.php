<?php

use Illuminate\Database\Seeder;
use App\Package;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Package::truncate();
        
        Package::create([
            'name'=>'Basic',
            'monthly_price'=>'50',
            'yearly_price'=>'50'
            ]);
        Package::create([
            'name'=>'Standard',
            'monthly_price'=>'80',
            'yearly_price'=>'50'
            ]);
    }
}
