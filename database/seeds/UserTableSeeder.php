<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::truncate();
        
        // $faker = \Faker\Factory::create();

        User::create([
            'name' => 'Irtaza',
            'email' => 'irtaza.qureshi@hotmail.com',
            'password' => Hash::make('admin123'),
            'role_id' => 1,
            'company_name' => 'amaxza',
            'phone' => '346350',
            'is_signup_complete' => true,
            'is_verification_complete' => true
        ]);
    
    }
}
