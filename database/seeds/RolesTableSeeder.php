<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        //
        Role::create([
            'role_id'=>'1',
            'role'=>'Administrator'
            ]);
        Role::create([
            'role_id'=>'2',
            'role'=>'Client'
            ]);
        Role::create([
            'role_id'=>'3',
            'role'=>'Employee'
            ]);
    }
}
