<?php

use Illuminate\Database\Seeder;
use App\Marketplace;

class MarketPlaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Marketplace::create([
            'marketplace_id' => 1,
            'marketplace_name'=>'Amazon.ca',
            'marketplace_value'=>'A2EUQ1WTGCTBG2'
        ]);
        Marketplace::create([
            'marketplace_id' => 2,
            'marketplace_name'=>'Amazon US',
            'marketplace_value'=>'ATVPDKIKX0DER'
        ]);
    }
}
