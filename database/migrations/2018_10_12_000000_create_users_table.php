<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->bigIncrements('user_id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->bigInteger('role_id')->unsigned()->index()->default(2);
            $table->foreign('role_id')->references('role_id')->on('roles');
            $table->string('company_name')->nullable();
            $table->string('phone');
            $table->string('temp');
            $table->bigInteger('client_id');
            $table->boolean('is_signup_complete')->default(0);
            $table->boolean('is_email_confirmed')->default(0);
            $table->boolean('is_platform_connected')->default(0);
            $table->boolean('is_verification_complete')->default(0);
            $table->boolean('is_blocked')->default(0);
            $table->bigIncrements('attempt');
            $table->timestamp('attempt_at')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
