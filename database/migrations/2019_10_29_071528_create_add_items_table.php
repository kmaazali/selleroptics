<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->string('asin');
            $table->double('list_price')->nullable();
            $table->string('category');
            $table->string('image');
            $table->string('title');
            $table->string('rank');
            $table->string('my_price');
            $table->integer('quantity');
            $table->string('msku')->unique();
            $table->string('condition');
            $table->json('note');
            $table->string('tax_code');
            $table->double('my_cost');
            $table->double('fees');
            $table->double('profit_loss');
            $table->integer('item_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_items');
    }
}
