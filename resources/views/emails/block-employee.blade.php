<html>
<b>Hi {{$data['client_name']}},</b><br><br>

Seller Optics is blocking {{$data['employee_name']}} account due to multiple unauthorized accesses.
<br>

<br>
If you have questions, problems, or need guidance of any kind, please visit our support site at http://help.selleroptics.com and our excellent support team will help you out.

Best Regards,<br>
The Seller Optics Support Team
</html>