<html>
<b>Hi {{$data['name']}},</b><br><br>

Seller Optics has been provided this email address for the purpose of resetting your password on our website. Please verify ownership by clicking the link below.
<br>

<a href="{{url('/reset-password').'?id='.$data['id']}}">Click Here</a>

<br>
If you have questions, problems, or need guidance of any kind, please visit our support site at http://help.selleroptics.com and our excellent support team will help you out.

Best Regards,<br>
The Seller Optics Support Team
</html>