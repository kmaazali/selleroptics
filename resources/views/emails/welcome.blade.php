<html>
<b>Hi {{$data['name']}},</b><br><br>

Seller Optics has been provided this email address for the purpose of signing up on our website. To enable us to create your account, please verify ownership by clicking the link below.

<a href="{{ url('/api/verifyaccount/'.$data['id']) }}">Click Here</a>

If you have questions, problems, or need guidance of any kind, please visit our support site at http://help.selleroptics.com and our excellent support team will help you out.

Best Regards,<br>
The Seller Optics Support Team
</html>
