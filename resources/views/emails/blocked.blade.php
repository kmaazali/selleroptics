<html>
<b>Hi {{$data['name']}},</b><br><br>

Seller Optics is blocking your account due to multiple unauthorized accesses.
<br>

<br>
If you have questions, problems, or need guidance of any kind, please visit our support site at http://help.selleroptics.com and our excellent support team will help you out.

Best Regards,<br>
The Seller Optics Support Team
</html>