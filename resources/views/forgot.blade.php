<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">

    <!-- Page plugins -->
    <link href="{{asset('assets/css/slick.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/slick-theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/owl.theme.css')}}" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css">
    <!-- Google Tag Manager -->

    <title>Seller Optics</title>
</head>
<body>
    <!-- Begins: Logo Header -->
    <section class="logoSec">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="logosvg">
              <a href="{{url('/')}}">
              <img src="{{asset('utils/images/logo.svg')}}" class="img-responsive" alt="logo">
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End: Logo Header -->
    <!-- Begins: Wellcome Sec -->
    <section class="wellcomeSec">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
            <div class="wellcomeBack">
              <h2>Forgot Password</h2>
              <p>Don't worry. Just enter your email and we'll send you a password reset link directly to your mailbox.</p>
            </div>
            <div class="wellcomeEmail">
              <form action="{{url('/forgetpassword')}}" method="post">
              @csrf
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <span class="fa mail_ico"></span>                                                
                  <input type="email" placeholder="Email" name="email" class="form-control" required>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="loginbtn">
                    <button type="submit" >SEND EMAIL</button>
                  </div>
                </div>
              </form>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="orSign">
                    <p>or</p>
                  </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="accountCreate">
                    <h4>Back to  <a href="{{url('/')}}">Sign in</a></h4>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End: Wellcome Sec -->
    <!-- Begins: footer Sec -->
    <section class="footerlogin">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <ul>
              <li><a href="#">About</a></li>
              <li><a href="#">Terms</a></li>
              <li><a href="#">Privacy</a></li>
            </ul>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <p>2019. Selleroptics Inc.</p>
          </div>
        </div>
      </div>
    </section>
    <!-- Ends: footer Sec -->
    <!-- Core -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <script charset="utf-8" src="{{asset('assets/vendor/bootstrap/dist/js/slick.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/vendor/jquery/dist/owl.carousel.js')}}"></script>
    <script src="{{asset('assets/vendor/jquery/dist/custom.js')}}"></script>
</body>
</html>
