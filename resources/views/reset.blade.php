<!DOCTYPE html>
<html>
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>..:: Admin Panel ::..</title>
    <!-- Favicon -->
    <!-- <link rel="icon" href="../../assets/img/brand/favicon.png" type="image/png"> -->
    <link rel="stylesheet" href="{{asset('assets/vendor/fortawesome/fontawesome-free/css/all.min.css')}}" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <!-- Page plugins -->
    <link href="{{asset('assets/css/slick.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/slick-theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/owl.theme.css')}}" rel="stylesheet">
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css">
    <!-- Google Tag Manager -->
  </head>
  <body>
    <!-- Begins: Logo Header -->
    <section class="logoSec">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="logosvg">
              <a href="{{url('/')}}">
              <img src="{{asset('utils/images/logo.svg')}}" class="img-responsive" alt="logo">
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End: Logo Header -->
    <!-- Begins: Wellcome Sec -->
    <section class="wellcomeSec loginSec">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
            <div class="wellcomeBack">
              <h2>Create New Password</h2>
              <p>Please enter your new password and we will update it for you.</p>
            </div>
            <form action="{{url('/resetpassword')}}" method="post">
            @csrf 
              <div class="wellcomeEmail">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <span class="pwd_ico"></span>                                                   
                  <input type="password" placeholder="Password" name="password" class="form-control">
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <span class="pwd_ico"></span>                                                
                  <input type="password" placeholder="Confirm Password" name="password" class="form-control">
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="loginbtn">
                    <button type="submit" >UPDATE PASSWORD</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- End: Wellcome Sec -->
    <!-- Begins: footer Sec -->
    <section class="footerlogin">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <ul>
              <li><a href="#">About</a></li>
              <li><a href="#">Terms</a></li>
              <li><a href="#">Privacy</a></li>
            </ul>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <p>2019. Selleroptics Inc.</p>
          </div>
        </div>
      </div>
    </section>
    <!-- Ends: footer Sec -->
    <!-- Core -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <script charset="utf-8" src="{{asset('assets/vendor/bootstrap/dist/js/slick.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/vendor/jquery/dist/owl.carousel.js')}}"></script>
    <script src="{{asset('assets/vendor/jquery/dist/custom.js')}}"></script>
  </body>
</html>