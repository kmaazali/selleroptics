<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">

    <!-- Page plugins -->
    <link href="{{asset('assets/css/slick.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/slick-theme.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/vendor/fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css" rel="stylesheet" type="text/css" />
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/flags-dropdown.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}" type="text/css">
    
    <title>Seller Optics</title>
</head>
<body>

<div id="app"></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="{{asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script charset="utf-8" src="{{asset('assets/vendor/bootstrap/dist/js/slick.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendor/jquery/dist/owl.carousel.js')}}"></script>
<script src="{{asset('assets/vendor/jquery/dist/custom.js')}}"></script>

<script src="./js/app.js"></script>

<script src="{{asset('js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script> -->
<!-- <script src="{{asset('/js/flags-dropdown.js')}}"></script> -->
<script type="text/javascript">
    $(document).ready(function() {
        $(document).bind("contextmenu", function (e) {
            e.preventDefault();
        });
        if (window.location.href.indexOf('mws') > 0) {
            document.getElementById("SelectPlanTable").setAttribute("selected", "standard");
        }
    });
    var url = window.location.origin+'/forgot';
    $('a#forgot').attr('href', url);
</script>

</body>
</html>