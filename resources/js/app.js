import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './App.vue';
import Dashboard from './components/Dashboard.vue';
import Register from './components/Register.vue';
import Login from './components/Login.vue';
import ThankYou from './components/ThankYou';
import ForgetPassword from './components/ForgetPassword';
import ResetPassword from './components/ResetPassword';
import ConfirmEmail from './components/ConfirmEmail';
import MWSAuth from './components/MWSAuth';
import SelectPlan from './components/SelectPlan';
import ItemPriceGrading from './components/ItemPriceGrading.vue';
import ItemGrading from './components/ItemGrading.vue';
import ListItemDashboard from './components/ListItemDashboard.vue';
import Logout from './components/Logout';

import AdminDashboard from './components/Admin/Dashboard.vue';
import AdminAddEmployee from './components/Admin/AddEmployee.vue';
import Users from './components/Admin/Users.vue';

window.$ = jQuery;
var cors = require('cors');

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(cors);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
const token=localStorage.getItem("token");
axios.defaults.baseURL = 'http://localhost:8000/api/';
axios.defaults.path_price_grade = 'http://localhost:8000/#/item-price-grading';
axios.defaults.path_grade = 'http://localhost:8000/#/item-grading';
axios.defaults.path_item_price_grading = 'http://localhost:8000/#/item-price-grading';
axios.defaults.headers.common['Authorization'] = 'Bearer '+token;

const router = new VueRouter({
    routes: [
        {
            path: '/',
            name: 'login',
            component: Login,
            meta: {
                title: 'Login - Seller Optics',
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                title: 'Login - Seller Optics',
            }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                auth: false,
                title: 'Register - Seller Optics'
            }
        },
        {
            path: '/thankyou',
            name: 'thankyou',
            component: ThankYou,
            meta: {
                auth: false,
                title: 'Thank You - Seller Optics'
            }
        },
        {
            path: '/forgotpassword',
            name: 'forgotpassword',
            component: ForgetPassword,
            meta: {
                auth: false,
                title: 'Forgot Password - Seller Optics'
            }
        },
        {
            path: '/resetpassword',
            name: 'resetpassword',
            component: ResetPassword,
            meta: {
                auth: false,
                title: 'Forgot Password - Seller Optics'
            }
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                auth: false,
                title: 'Dashboard - Seller Optics'
            }
        },
        {
            path: '/confirmemail',
            name: 'confirmemail',
            component: ConfirmEmail,
            meta: {
            auth: false,
            title: 'Confirm Your Email - Seller Optics'
            }
        },
        {
            path: '/mwsauthorization',
            name: 'mwsauthorization',
            component: MWSAuth,
            meta: {
                auth:false,
                title: 'MWS Authorization - Seller Optics'
            }
        },
        {
            path: '/selectplan',
            name: 'selectplan',
            component: SelectPlan,
            meta: {
                auth:false,
                title: 'Select Plan - Seller Optics'
            }
        },
        {
            path: '/item-price-grading',
            name: 'ItemPriceGrading',
            component: ItemPriceGrading,
            meta: {
                auth:false,
                title: 'Item Pricing - Seller Optics'
            }
        },
        {
            path: '/item-grading',
            name: 'ItemGrading',
            component: ItemGrading,
            meta: {
                auth:false,
                title: 'Item Pricing - Seller Optics'
            }
        },
        {
            path: '/list-item-dashboard',
            name: 'ListItemDashboard',
            component: ListItemDashboard,
            meta: {
                auth:false,
                title: 'List Item Dashboard - Seller Optics'
            }
        },
        {
            path: '/logout',
            name: 'logout',
            component: Logout,
            meta:{
                auth: false,
            }
        },

        //Admin dashboard routes

        {
            path: '/admin-dashboard',
            name: 'admin-dashboard',
            component: AdminDashboard,
            meta: {
                title: 'Admin Dashboard - Seller Optics',
            }
        },
        {
            path: '/add-employee',
            name: 'add-employee',
            component: AdminAddEmployee,
            meta: {
                title: 'Add Employee - Seller Optics',
            }
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
            meta: {
                title: 'Users - Seller Optics',
            }
        },
    ]
});

Vue.router = router
Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});
App.router = Vue.router
new Vue(App).$mount('#app');


// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

    // Skip rendering meta tags if there are none.
    if(!nearestWithMeta) return next();

    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });

        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    })
    // Add the meta tags to the document head.
        .forEach(tag => document.head.appendChild(tag));

    next();
});